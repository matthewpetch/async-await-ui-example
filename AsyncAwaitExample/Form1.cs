﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AsyncAwaitExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void OutputText(string value)
        {
            if (textBox1.InvokeRequired)
            {
                textBox1.Invoke(new Action(() =>
                {
                    textBox1.AppendText(Environment.NewLine + value);
                }));
            }
            else
            {
                textBox1.AppendText(Environment.NewLine + value);
            }
        }

        private string RandomString()
        {
            return System.IO.Path.GetRandomFileName().Replace(".", "");
        }

        private string LongRunningString()
        {
            Thread.Sleep(2000); 
            return RandomString();
        }

        private async Task<string> LongRunningStringAsync()
        {
            await Task.Delay(2000);
            return RandomString();
        }

        private void outputTextButton_Click(object sender, EventArgs e)
        {
            OutputText("Output text button: " + RandomString());
        }

        private void longBlockingButton_Click(object sender, EventArgs e)
        {
            OutputText("Long-running button: " + LongRunningString());
        }

        private async void longNonBlockingButton_Click(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                OutputText("Long-running async button: " + LongRunningString());
            });

            //await DoSomethingAsync();
            //await DoSomethingElsewhereAsync();
        }


        // Alternatively, for the above event handler, you could simply await another method
        // this will call OutputText on the UI thread, but await the long running operation on another thread first
        private async Task DoSomethingAsync()
        {
            OutputText("Async DoSomething: " + await LongRunningStringAsync());
        }

        // Or even
        // this will call both OutputText and the long-running operation on another thread
        private Task DoSomethingElsewhereAsync()
        {
            return Task.Run(async () =>
            {
                OutputText("Long-running async button: " + await LongRunningStringAsync());
            });
        }
    }

    
}
