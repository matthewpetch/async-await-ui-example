﻿using DevExpress.XtraEditors;

namespace AsyncAwaitExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.outputTextButton = new DevExpress.XtraEditors.SimpleButton();
            this.longBlockingButton = new DevExpress.XtraEditors.SimpleButton();
            this.longNonBlockingButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.marqueeProgressBarControl1.EditValue = "UI thread";
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(13, 231);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(259, 18);
            this.marqueeProgressBarControl1.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(13, 13);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(259, 75);
            this.textBox1.TabIndex = 1;
            // 
            // outputTextButton
            // 
            this.outputTextButton.AutoSize = true;
            this.outputTextButton.Location = new System.Drawing.Point(13, 100);
            this.outputTextButton.Name = "outputTextButton";
            this.outputTextButton.Size = new System.Drawing.Size(66, 22);
            this.outputTextButton.TabIndex = 2;
            this.outputTextButton.Text = "Output text";
            this.outputTextButton.Click += new System.EventHandler(this.outputTextButton_Click);
            // 
            // longBlockingButton
            // 
            this.longBlockingButton.AutoSize = true;
            this.longBlockingButton.Location = new System.Drawing.Point(13, 130);
            this.longBlockingButton.Name = "longBlockingButton";
            this.longBlockingButton.Size = new System.Drawing.Size(179, 22);
            this.longBlockingButton.TabIndex = 3;
            this.longBlockingButton.Text = "Long-running output text (blocking)";
            this.longBlockingButton.Click += new System.EventHandler(this.longBlockingButton_Click);
            // 
            // longNonBlockingButton
            // 
            this.longNonBlockingButton.AutoSize = true;
            this.longNonBlockingButton.Location = new System.Drawing.Point(13, 160);
            this.longNonBlockingButton.Name = "longNonBlockingButton";
            this.longNonBlockingButton.Size = new System.Drawing.Size(169, 22);
            this.longNonBlockingButton.TabIndex = 4;
            this.longNonBlockingButton.Text = "Long-running output text (async)";
            this.longNonBlockingButton.Click += new System.EventHandler(this.longNonBlockingButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.longNonBlockingButton);
            this.Controls.Add(this.longBlockingButton);
            this.Controls.Add(this.outputTextButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.marqueeProgressBarControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private System.Windows.Forms.TextBox textBox1;
        private SimpleButton outputTextButton;
        private SimpleButton longBlockingButton;
        private SimpleButton longNonBlockingButton;
    }
}

