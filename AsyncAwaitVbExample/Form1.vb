﻿Imports System.Threading

Public Class Form1

    Private Sub OutputText(value As String)
        If TextBox1.InvokeRequired Then
            TextBox1.Invoke(New Action(Sub()
                                           TextBox1.AppendText(Environment.NewLine + value)
                                       End Sub))
        Else
            TextBox1.AppendText(Environment.NewLine + value)
        End If
    End Sub

    Private Function RandomString() As String
        Return IO.Path.GetRandomFileName().Replace(".", "")
    End Function

    Private Function LongRunningString() As String
        Thread.Sleep(2000)
        Return RandomString()
    End Function

    Private Async Function LongRunningStringAsync() As Task(Of String)
        Await Task.Delay(2000)
        Return RandomString()
    End Function

    Private Sub outputTextButton_Click(sender As Object, e As EventArgs) Handles outputTextButton.Click
        OutputText("Output text button: " & RandomString())
    End Sub

    Private Sub longBlockingButton_Click(sender As Object, e As EventArgs) Handles longBlockingButton.Click
        OutputText("Long-running button: " & LongRunningString())
    End Sub

    Private Async Sub longNonBlockingButton_Click(sender As Object, e As EventArgs) Handles longNonBlockingButton.Click
        Await Task.Run(Sub()
                           OutputText("Long-running async button: " & LongRunningString())
                       End Sub)

        'Await DoSomethingAsync()
        'Await DoSomethingElsewhereAsync()
    End Sub

    ' Alternatively, for the above event handler, you could simply await another method
    ' this will call OutputText on the UI thread, but await the long running operation on another thread first
    Private Async Function DoSomethingAsync() As Task
        OutputText("Async DoSomething: " & Await LongRunningStringAsync())
    End Function

    ' Or even...
    'this will call both OutputText And the long-running operation on another thread
    Private Function DoSomethingElsewhereAsync() As Task
        Return Task.Run(Async Function()
                            OutputText("Long-running async button: " & Await LongRunningStringAsync())
                        End Function)
    End Function
End Class
