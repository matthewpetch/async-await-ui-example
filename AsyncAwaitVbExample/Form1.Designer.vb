﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MarqueeProgressBarControl1 = New DevExpress.XtraEditors.MarqueeProgressBarControl()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.outputTextButton = New DevExpress.XtraEditors.SimpleButton()
        Me.longBlockingButton = New DevExpress.XtraEditors.SimpleButton()
        Me.longNonBlockingButton = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MarqueeProgressBarControl1
        '
        Me.MarqueeProgressBarControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MarqueeProgressBarControl1.EditValue = 0
        Me.MarqueeProgressBarControl1.Location = New System.Drawing.Point(12, 231)
        Me.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1"
        Me.MarqueeProgressBarControl1.Size = New System.Drawing.Size(260, 18)
        Me.MarqueeProgressBarControl1.TabIndex = 0
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(12, 12)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(260, 75)
        Me.TextBox1.TabIndex = 1
        '
        'outputTextButton
        '
        Me.outputTextButton.AutoSize = True
        Me.outputTextButton.Location = New System.Drawing.Point(13, 94)
        Me.outputTextButton.Name = "outputTextButton"
        Me.outputTextButton.Size = New System.Drawing.Size(66, 22)
        Me.outputTextButton.TabIndex = 2
        Me.outputTextButton.Text = "Output text"
        '
        'longBlockingButton
        '
        Me.longBlockingButton.AutoSize = True
        Me.longBlockingButton.Location = New System.Drawing.Point(13, 123)
        Me.longBlockingButton.Name = "longBlockingButton"
        Me.longBlockingButton.Size = New System.Drawing.Size(179, 22)
        Me.longBlockingButton.TabIndex = 3
        Me.longBlockingButton.Text = "Long-running output text (blocking)"
        '
        'longNonBlockingButton
        '
        Me.longNonBlockingButton.AutoSize = True
        Me.longNonBlockingButton.Location = New System.Drawing.Point(13, 153)
        Me.longNonBlockingButton.Name = "longNonBlockingButton"
        Me.longNonBlockingButton.Size = New System.Drawing.Size(118, 22)
        Me.longNonBlockingButton.TabIndex = 4
        Me.longNonBlockingButton.Text = "longNonBlockingButton"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.longNonBlockingButton)
        Me.Controls.Add(Me.longBlockingButton)
        Me.Controls.Add(Me.outputTextButton)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.MarqueeProgressBarControl1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MarqueeProgressBarControl1 As DevExpress.XtraEditors.MarqueeProgressBarControl
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents outputTextButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents longBlockingButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents longNonBlockingButton As DevExpress.XtraEditors.SimpleButton
End Class
